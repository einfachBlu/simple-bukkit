package de.blu.inventory.menu;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Menu implements Listener {

    @Getter
    private Inventory inventory;

    @Getter
    private JavaPlugin plugin;

    public Menu(JavaPlugin plugin, int size, String title) {
        this.plugin = plugin;
        this.inventory = Bukkit.createInventory(null, size, title.length() > 32 ? title.substring(0, 32) : title);

        // Register Listener
        this.getPlugin().getServer().getPluginManager().registerEvents(this, this.getPlugin());
    }

    protected void onClose(InventoryCloseEvent e) {
    }

    protected void onOpen(InventoryOpenEvent e) {
    }

    protected void onClick(InventoryClickEvent e) {
    }

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent e) {
        if (!e.getInventory().getTitle().equals(this.getInventory().getTitle()) || e.getInventory().getSize() != this.getInventory().getSize()) {
            return;
        }

        this.onOpen(e);
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        if (!e.getInventory().getTitle().equals(this.getInventory().getTitle()) || e.getInventory().getSize() != this.getInventory().getSize()) {
            return;
        }

        this.onClose(e);
    }

    @EventHandler
    public void onClickEvent(InventoryClickEvent e) {
        this.onClick(e);
    }

    public void open(Player player) {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (player == null) {
                    throw new IllegalArgumentException("player is null");
                }

                if (Menu.this.getInventory() == null) {
                    throw new IllegalArgumentException("inventory is null");
                }

                player.openInventory(Menu.this.getInventory());
            }
        }.runTaskLater(this.getPlugin(), 1);
    }
}
