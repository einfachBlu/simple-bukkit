package de.blu.inventory.util;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryHelper {

    /**
     * fill all slots in an inventory with an item
     *
     * @param inventory inventory
     * @param item      item
     */
    public static void fill(Inventory inventory, ItemStack item) {
        for (int i = 0; i < inventory.getSize(); i++) {
            inventory.setItem(i, item);
        }
    }

    /**
     * fill all slots in a line with an item
     *
     * @param inventory inventory
     * @param item      item
     * @param row       row, starting from 1
     */
    public static void line(Inventory inventory, ItemStack item, int row) {
        for (int i = 0; i < 9; i++) {
            inventory.setItem((row - 1) * 9 + i, item);
        }
    }

    /**
     * fill the border of an inventory with an item
     *
     * @param inventory inventory
     * @param item      item
     */
    public static void border(Inventory inventory, ItemStack item) {
        for (int row = 0; row < inventory.getSize() / 9; row++) {
            if (row == 0 || row == inventory.getSize() / 9 - 1) {
                line(inventory, item, row + 1);
                continue;
            }
            inventory.setItem(row * 9, item);
            inventory.setItem(row * 9 + 8, item);
        }
    }
}
