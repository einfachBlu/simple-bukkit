package de.blu.simplebukkit;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import de.blu.simplebukkit.command.CommandExecutor;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.event.Listener;
import org.bukkit.plugin.SimplePluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class BukkitHelper {

    @Getter
    private static Injector injector;

    @Getter
    private static JavaPlugin javaPlugin;

    @Getter
    private static Collection<Class<?>> registeredListeners = new HashSet<>();

    @Getter
    private static Collection<Class<?>> registeredCommands = new HashSet<>();

    public static Injector init(JavaPlugin javaPlugin, Consumer<Binder> bindingAction) {
        AbstractModule module = new AbstractModule() {
            @Override
            protected void configure() {
                bind(JavaPlugin.class).toInstance(javaPlugin);
                bind(ExecutorService.class).toInstance(Executors.newCachedThreadPool());
                bind(ListeningExecutorService.class).toInstance(MoreExecutors.listeningDecorator(Executors.newCachedThreadPool()));

                bindingAction.accept(this.binder());
            }
        };

        BukkitHelper.javaPlugin = javaPlugin;
        BukkitHelper.injector = Guice.createInjector(module);
        return BukkitHelper.getInjector();
    }

    public static void registerListenersRecursive(String packageName) {
        if (BukkitHelper.getInjector() == null) {
            BukkitHelper.getJavaPlugin().getLogger().severe("You need to call BukkitHelper#init() before using other Methods!");
            return;
        }

        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage(packageName, BukkitHelper.getJavaPlugin().getClass().getClassLoader()))
                .setScanners(new SubTypesScanner(false)));
        for (Class<?> listenerClass : reflections.getSubTypesOf(Listener.class)) {
            if (BukkitHelper.getRegisteredListeners().contains(listenerClass)) {
                continue;
            }

            if (!listenerClass.getName().toLowerCase().startsWith(packageName.toLowerCase())) {
                continue;
            }

            try {
                Listener listener = (Listener) BukkitHelper.getInjector().getInstance(listenerClass);
                BukkitHelper.getInjector().injectMembers(listener);

                BukkitHelper.getJavaPlugin().getServer().getPluginManager().registerEvents(listener, BukkitHelper.getJavaPlugin());
                System.out.println("Registered Listener " + listener.getClass().getSimpleName());
                BukkitHelper.getRegisteredListeners().add(listenerClass);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void registerCommandsRecursive(String packageName) {
        if (BukkitHelper.getInjector() == null) {
            BukkitHelper.getJavaPlugin().getLogger().severe("You need to call BukkitHelper#init() before using other Methods!");
            return;
        }

        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage(packageName, BukkitHelper.getJavaPlugin().getClass().getClassLoader(), ClassLoader.getSystemClassLoader(), Bukkit.getServer().getClass().getClassLoader()))
                .setScanners(new SubTypesScanner(false)));
        for (Class<?> commandClass : reflections.getSubTypesOf(CommandExecutor.class)) {
            if (BukkitHelper.getRegisteredCommands().contains(commandClass)) {
                continue;
            }

            if (!commandClass.getName().toLowerCase().startsWith(packageName.toLowerCase())) {
                continue;
            }

            try {
                CommandExecutor commandExecutor = (CommandExecutor) BukkitHelper.getInjector().getInstance(commandClass);
                BukkitHelper.getInjector().injectMembers(commandClass);

                if (Bukkit.getPluginManager() instanceof SimplePluginManager) {
                    Field f = Bukkit.getPluginManager().getClass().getDeclaredField("commandMap");
                    f.setAccessible(true);
                    CommandMap commandMap = (CommandMap) f.get(Bukkit.getPluginManager());
                    f.setAccessible(false);

                    commandMap.register(commandExecutor.getName(), commandExecutor.getName(), commandExecutor);
                    BukkitHelper.getRegisteredCommands().add(commandClass);
                    System.out.println("Registered Command " + commandExecutor.getName() + ", Aliases=" + Arrays.toString(commandExecutor.getAliases().toArray()));
                } else {
                    BukkitHelper.getJavaPlugin().getLogger().warning("SimplePluginManager is not available.");
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
