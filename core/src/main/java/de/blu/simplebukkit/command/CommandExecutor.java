package de.blu.simplebukkit.command;

import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

@Getter
public abstract class CommandExecutor extends Command {

    public CommandExecutor(String name) {
        super(name);
    }

    public CommandExecutor(String name, String description, String usageMessage, List<String> aliases) {
        super(name, description, usageMessage, aliases);
    }

    @Override
    public final boolean execute(CommandSender sender, String s, String[] args) {
        // We only have to check the permission
        if (!super.testPermissionSilent(sender)) {
            sendNoPerm(sender);
            return true;
        }

        if (sender instanceof Player) {
            run((Player) sender, args);
        } else {
            run(sender, args);
        }

        return true;
    }

    protected void sendNoPerm(CommandSender sender) {
        sender.sendMessage(ChatColor.RED + "I'm sorry, but you do not have permission to perform this command. Please contact the server administrators if you believe that is in error.");
    }

    public void run(CommandSender sender, String[] args) {
        sender.sendMessage("This command cannot be executed by the console");
    }

    public void run(Player player, String[] args) {
        player.sendMessage("This command cannot be executed by players");
    }
}
