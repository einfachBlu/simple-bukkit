package de.blu.simplebukkit.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AccessLevel;
import lombok.Getter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UUIDFetcher {

    private static final String UUID_URL = "https://api.mojang.com/users/profiles/minecraft/%s";
    private static final String NAME_URL = "https://api.mojang.com/user/profiles/%s/names";
    private static Gson gson = new GsonBuilder().registerTypeAdapter(UUID.class, new UUIDTypeAdapter()).create();

    @Getter(AccessLevel.PUBLIC)
    private static Map<String, UUID> uuidCache = new HashMap<>();

    @Getter(AccessLevel.PUBLIC)
    private static Map<UUID, String> nameCache = new HashMap<>();

    public static String getName(UUID uuid) {
        if (UUIDFetcher.getNameCache().containsKey(uuid)) {
            return UUIDFetcher.getNameCache().get(uuid);
        }

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(String.format(UUIDFetcher.NAME_URL, UUIDTypeAdapter.fromUUID(uuid))).openConnection();
            connection.setReadTimeout(5000);

            Data[] nameHistory = UUIDFetcher.gson.fromJson(new BufferedReader(new InputStreamReader(connection.getInputStream())), Data[].class);
            Data currentNameData = nameHistory[nameHistory.length - 1];

            if (nameHistory.length == 0 || currentNameData == null) {
                return null;
            }

            // Cache local
            UUIDFetcher.getNameCache().put(uuid, currentNameData.name);
            UUIDFetcher.getUuidCache().put(currentNameData.name.toLowerCase(), uuid);

            return currentNameData.name;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static UUID getUUID(String name) {
        if (UUIDFetcher.getUuidCache().containsKey(name.toLowerCase())) {
            return UUIDFetcher.getUuidCache().get(name.toLowerCase());
        }

        try {
            URL api = new URL(String.format(UUIDFetcher.UUID_URL, name));
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.openStream()));
            Data data = UUIDFetcher.gson.fromJson(reader, Data.class);

            if (data == null) {
                return null;
            }

            // Cache local
            UUIDFetcher.getUuidCache().put(data.name.toLowerCase(), data.id);
            UUIDFetcher.getNameCache().put(data.id, data.name);

            return data.id;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private class Data {

        private String name;
        private UUID id;

        @Override
        public String toString() {
            return "Data{" +
                    "name='" + name + '\'' +
                    ", id=" + id +
                    '}';
        }
    }
}
