package de.blu.simplebukkit.data;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.Map;

public interface KeyValueRepository<K, V> {

    Map<K, V> get();

    ListenableFuture<Map<K, V>> getAsync();

    void load();

    void put(K key, V value);

    void remove(K key);
}
