package de.blu.simplebukkit.data;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.Collection;

public interface Repository<T> {

    Collection<T> get();

    ListenableFuture<Collection<T>> getAsync();

    void load();

    void add(T element);

    void remove(T element);
}
