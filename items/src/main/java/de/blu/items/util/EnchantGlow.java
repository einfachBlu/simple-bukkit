package de.blu.items.util;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;

public class EnchantGlow extends Enchantment {

    private static Enchantment glow;

    public EnchantGlow(int id) {
        super(id);
    }

    public static Enchantment getGlow() {
        if (glow != null) {
            return glow;
        }

        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        glow = new EnchantGlow(255);
        Enchantment.registerEnchantment(glow);
        return glow;
    }

    public static void addGlow(ItemStack item) {
        Enchantment glow = getGlow();

        item.addEnchantment(glow, 1);
    }

    public static void removeGlow(ItemStack item) {
        Enchantment glow = getGlow();
        item.removeEnchantment(glow);
    }

    public boolean canEnchantItem(ItemStack item) {
        return true;
    }

    @Override
    public boolean conflictsWith(Enchantment other) {
        return false;
    }

    @Override
    public EnchantmentTarget getItemTarget() {
        return null;
    }

    @Override
    public int getMaxLevel() {
        return 1;
    }

    @Override
    public String getName() {
        return " ";
    }

    @Override
    public int getId() {
        return 255;
    }

    @Override
    public int getStartLevel() {
        return 1;
    }
}
