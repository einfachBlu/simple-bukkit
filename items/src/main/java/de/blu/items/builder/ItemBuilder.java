package de.blu.items.builder;

import com.google.inject.Injector;
import de.blu.items.ItemsModule;
import de.blu.items.util.EnchantGlow;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ItemBuilder {

    @Getter
    @Setter
    private static Injector injector;

    @Getter
    @Setter
    private static ItemsModule itemsModule;

    @Getter
    @Setter
    private ItemStack item;

    @Getter
    @Setter
    private ItemMeta itemMeta;

    public ItemBuilder() {
        this.item = new ItemStack(Material.AIR);
        this.itemMeta = this.getItem().getItemMeta();
    }

    public static ItemBuilder air() {
        return ItemBuilder.getInjector().getInstance(ItemBuilder.class).setType(Material.AIR);
    }

    public static BookItemBuilder book() {
        return ItemBuilder.getInjector().getInstance(BookItemBuilder.class);
    }

    public static BookItemBuilder book(boolean writeable) {
        return ItemBuilder.getInjector().getInstance(BookItemBuilder.class).setWriteable(writeable);
    }

    public static PotionItemBuilder potion() {
        return ItemBuilder.getInjector().getInstance(PotionItemBuilder.class);
    }

    public static DefaultsItemBuilder defaults() {
        return ItemBuilder.getInjector().getInstance(DefaultsItemBuilder.class);
    }

    public static LeatherArmorItemBuilder armor() {
        return ItemBuilder.getInjector().getInstance(LeatherArmorItemBuilder.class);
    }

    public static LeatherArmorItemBuilder armor(LeatherArmorItemBuilder.ArmorElement armorElement) {
        return (LeatherArmorItemBuilder) ItemBuilder.getInjector().getInstance(LeatherArmorItemBuilder.class).setType(armorElement.getMaterial());
    }

    public static SkullItemBuilder skull() {
        return ItemBuilder.getInjector().getInstance(SkullItemBuilder.class);
    }

    public static SkullItemBuilder skull(SkullType skullType) {
        return (SkullItemBuilder) ItemBuilder.getInjector().getInstance(SkullItemBuilder.class).setDurability((short) skullType.ordinal());
    }

    public static ItemBuilder normal() {
        return ItemBuilder.getInjector().getInstance(ItemBuilder.class);
    }

    public static ItemBuilder normal(Material material) {
        return ItemBuilder.getInjector().getInstance(ItemBuilder.class).setType(material);
    }

    public static ItemBuilder normal(Material material, int amount) {
        return ItemBuilder.getInjector().getInstance(ItemBuilder.class).setType(material).setAmount(amount);
    }

    public static ItemBuilder normal(Material material, int amount, short durability) {
        return ItemBuilder.getInjector().getInstance(ItemBuilder.class).setType(material).setAmount(amount).setDurability(durability);
    }

    public static ItemBuilder wrap(ItemStack itemStack) {
        if (itemStack.getType().equals(Material.SKULL) || itemStack.getType().equals(Material.SKULL_ITEM)) {
            SkullItemBuilder skullItemBuilder = ItemBuilder.getInjector().getInstance(SkullItemBuilder.class);
            skullItemBuilder.setItem(itemStack);
            skullItemBuilder.setItemMeta(itemStack.getItemMeta());
            return skullItemBuilder;
        }

        ItemBuilder builder = ItemBuilder.getInjector().getInstance(ItemBuilder.class);
        builder.setItem(itemStack);
        builder.setItemMeta(itemStack.getItemMeta());
        return builder;
    }

    public static ItemBuilder copy(ItemStack itemStack) {
        return ItemBuilder.wrap(itemStack.clone());
    }

    public ItemStack build() {
        this.getItem().setItemMeta(this.getItemMeta());
        return this.getItem();
    }

    public ItemBuilder apply() {
        this.getItem().setItemMeta(this.getItemMeta());
        return this;
    }

    public ItemBuilder setType(Material material) {
        this.getItem().setType(material);
        this.setItemMeta(this.getItem().getItemMeta());
        return this;
    }

    public ItemBuilder setAmount(int amount) {
        this.getItem().setAmount(amount);
        return this;
    }

    public ItemBuilder setDurability(short durability) {
        this.getItem().setDurability(durability);
        return this;
    }

    public ItemBuilder setData(MaterialData data) {
        this.getItem().setData(data);
        return this;
    }

    public ItemBuilder setDisplayName(String displayName) {
        if (this.getItemMeta() == null) {
            return this;
        }

        this.getItemMeta().setDisplayName(displayName);
        return this;
    }

    public String getLoreEntry(int index) {
        if (this.getItemMeta() == null) {
            return "";
        }

        if (this.getItemMeta().getLore().size() <= index) {
            return "";
        }

        return this.getItemMeta().getLore().get(index);
    }

    public ItemBuilder setLoreEntry(int index, String line) {
        if (this.getItemMeta() == null) {
            return this;
        }

        if (index < 0) {
            return this;
        }

        List<String> lore = this.getItemMeta().getLore();

        if (lore.size() <= index) {
            for (int i = 0; i < index + 1; i++) {
                if (lore.size() <= i) {
                    lore.add("");
                }
            }
        }
        lore.set(index, line);
        this.getItemMeta().setLore(lore);

        return this;
    }

    public ItemBuilder clearLore() {
        if (this.getItemMeta() == null) {
            return this;
        }

        this.getItemMeta().setLore(new ArrayList<>());
        return this;
    }

    public List<String> getLore() {
        if (this.getItemMeta() == null) {
            return new ArrayList<>();
        }

        return this.getItemMeta().getLore();
    }

    public ItemBuilder setLore(String... lines) {
        this.setLore(Arrays.asList(lines));
        return this;
    }

    public ItemBuilder setLore(List<String> lines) {
        if (this.getItemMeta() == null) {
            return this;
        }

        this.getItemMeta().setLore(lines);
        return this;
    }

    public ItemBuilder addItemFlags(ItemFlag... itemFlags) {
        this.getItemMeta().addItemFlags(itemFlags);
        return this;
    }

    public ItemBuilder removeItemFlags(ItemFlag... itemFlags) {
        this.getItemMeta().removeItemFlags(itemFlags);
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment enchantment, int level) {
        this.getItemMeta().addEnchant(enchantment, level, true);
        return this;
    }

    public ItemBuilder removeEnchantment(Enchantment enchantment) {
        this.getItemMeta().removeEnchant(enchantment);

        return this;
    }

    public ItemBuilder setUnbreakable() {
        return this.setUnbreakable(true);
    }

    public boolean isUnbreakable() {
        return this.getItemMeta().spigot().isUnbreakable();
    }

    public ItemBuilder setUnbreakable(boolean value) {
        this.getItemMeta().spigot().setUnbreakable(value);
        return this;
    }

    public ItemBuilder addGlow() {
        EnchantGlow.addGlow(this.getItem());
        return this;
    }

    public ItemBuilder removeGlow() {
        EnchantGlow.removeGlow(this.getItem());
        return this;
    }

    public ItemBuilder setOnEntityHitListener(Consumer<EntityDamageByEntityEvent> listener) {
        this.build();

        int id = this.getItemsModule().getClickableItemId(this.getItem());
        if (id == -1) {
            this.item = this.getItemsModule().initClickableItem(this.getItem());
            this.itemMeta = this.getItem().getItemMeta();
        }

        this.getItemsModule().getEntityHitItemListener().put(id, listener);
        return this;
    }

    public ItemBuilder setOnEntityHitListener(Consumer<EntityDamageByEntityEvent> listener, long cooldown) {
        this.setOnEntityHitListener(listener);

        int id = this.getItemsModule().getClickableItemId(this.getItem());
        if (id == -1) {
            this.item = this.getItemsModule().initClickableItem(this.getItem());
            this.itemMeta = this.getItem().getItemMeta();
        }

        this.getItemsModule().getCooldown().put(this.getItemsModule().getClickableItemId(this.getItem()), cooldown);
        return this;
    }

    public ItemBuilder setOnRightClickListener(Consumer<PlayerInteractEvent> listener) {
        this.build();

        int id = this.getItemsModule().getClickableItemId(this.getItem());
        if (id == -1) {
            this.item = this.getItemsModule().initClickableItem(this.getItem());
            this.itemMeta = this.getItem().getItemMeta();
        }

        this.getItemsModule().getRightClickItemListener().put(this.getItemsModule().getClickableItemId(this.getItem()), listener);
        return this;
    }

    public ItemBuilder setOnRightClickListener(Consumer<PlayerInteractEvent> listener, long cooldown) {
        this.setOnRightClickListener(listener);

        int id = this.getItemsModule().getClickableItemId(this.getItem());
        if (id == -1) {
            this.item = this.getItemsModule().initClickableItem(this.getItem());
            this.itemMeta = this.getItem().getItemMeta();
        }

        this.getItemsModule().getCooldown().put(this.getItemsModule().getClickableItemId(this.getItem()), cooldown);
        return this;
    }

    public ItemBuilder setOnLeftClickListener(Consumer<PlayerInteractEvent> listener) {
        this.build();

        int id = this.getItemsModule().getClickableItemId(this.getItem());
        if (id == -1) {
            this.item = this.getItemsModule().initClickableItem(this.getItem());
            this.itemMeta = this.getItem().getItemMeta();
        }

        this.getItemsModule().getLeftClickItemListener().put(this.getItemsModule().getClickableItemId(this.getItem()), listener);
        return this;
    }

    public ItemBuilder setOnLeftClickListener(Consumer<PlayerInteractEvent> listener, long cooldown) {
        this.setOnLeftClickListener(listener);

        int id = this.getItemsModule().getClickableItemId(this.getItem());
        if (id == -1) {
            this.item = this.getItemsModule().initClickableItem(this.getItem());
            this.itemMeta = this.getItem().getItemMeta();
        }

        this.getItemsModule().getCooldown().put(this.getItemsModule().getClickableItemId(this.getItem()), cooldown);
        return this;
    }

    public ItemBuilder setOnInventoryClickListener(Consumer<InventoryClickEvent> listener) {
        this.build();

        int id = this.getItemsModule().getClickableItemId(this.getItem());
        if (id == -1) {
            this.item = this.getItemsModule().initClickableItem(this.getItem());
            this.itemMeta = this.getItem().getItemMeta();
        }

        id = this.getItemsModule().getClickableItemId(this.getItem());

        this.getItemsModule().getInventoryClickItemListener().put(id, listener);
        return this;
    }

    public ItemBuilder setOnInventoryClickListener(Consumer<InventoryClickEvent> listener, long cooldown) {
        this.setOnInventoryClickListener(listener);

        int id = this.getItemsModule().getClickableItemId(this.getItem());
        if (id == -1) {
            this.item = this.getItemsModule().initClickableItem(this.getItem());
            this.itemMeta = this.getItem().getItemMeta();
        }

        this.getItemsModule().getCooldown().put(this.getItemsModule().getClickableItemId(this.getItem()), cooldown);
        return this;
    }
}
