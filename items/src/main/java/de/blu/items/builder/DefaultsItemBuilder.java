package de.blu.items.builder;

import org.bukkit.DyeColor;
import org.bukkit.Material;

public class DefaultsItemBuilder extends ItemBuilder {

    /**
     * get a placeholder-glass
     *
     * @param color color of the glass
     * @return itembuilder
     */
    public DefaultsItemBuilder placeHolderGlass(DyeColor color) {
        this.setType(Material.STAINED_GLASS_PANE);
        this.setDisplayName("§7 ");
        this.setDurability(color.getWoolData());
        return this;
    }
}
